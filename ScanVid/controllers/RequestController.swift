//
//  RequestController.swift
//  ScanVid
//
//  Created by Mohammed Salah on 4/30/17.
//  Copyright © 2017 ScanVid. All rights reserved.
//

import UIKit
import Alamofire

class RequestController: NSObject {
    
    var delegate:NetworkScanSearchDelegate?
    
    public func searchForBarCode(code:String, deleg: NetworkScanSearchDelegate) {
        
        self.delegate = deleg
        Alamofire.request(Constants.Request.BAR_URL + code).validate().responseJSON { (response) -> Void in
            
            guard response.result.isSuccess else {
                print("Error while fetching remote rooms: \(response.result.error)")
                return
            }
            
            guard let value = response.result.value as? [String: Any],
                let items = value["itemsss"] as? [[String : Any]] else {
                    return
            }
            
            if items.count > 0 {
                let item = items[0]
                let title = item["itit"] as! String + ""
                let videoID = item ["iurl"] as! String + ""
                
                self.delegate?.requestSuccessWithData (title:title, videoId:(videoID.replacingOccurrences(of: "https://www.youtube.com/watch?v=", with: "")))
                
            }
            
        }
    }
    
    
    public func searchForQrCode(code:String, deleg: NetworkScanSearchDelegate) {
        
        self.delegate = deleg
        Alamofire.request(Constants.Request.QR_URL + code).validate().responseJSON { (response) -> Void in
            
            guard response.result.isSuccess else {
                print("Error while fetching remote rooms: \(response.result.error)")
                return
            }
            
            guard let value = response.result.value as? [String: Any],
                let items = value["itemsss"] as? [[String : Any]] else {
                    return
            }
            
            if items.count > 0 {
                let item = items[0]
                let title = item["itit"]
                let videoID = item ["iurl"] as? String
                
                self.delegate?.requestSuccessWithData (title:title as! String, videoId:(videoID?.replacingOccurrences(of: "https://www.youtube.com/watch?v=", with: ""))!)
                
            }
            
        }
    }


}
