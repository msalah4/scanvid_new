//
//  NetworkDelegate.swift
//  ScanVid
//
//  Created by Mohammed Salah on 4/30/17.
//  Copyright © 2017 ScanVid. All rights reserved.
//

import Foundation

protocol NetworkScanSearchDelegate {
    
    func requestSuccessWithData (title:String, videoId:String)
    
    func requestFailedWithError (error:String)
}

protocol ScanResultsDelegate {
    
    func codeFound ()
    func codeNotFound ()
    func networkError ()
}
