//
//  ScannerCodeHandler.swift
//  ScanVid
//
//  Created by Mohammed Salah on 4/30/17.
//  Copyright © 2017 ScanVid. All rights reserved.
//

import UIKit
import AVFoundation

class ScannerCodeHandler: NSObject {
    
    public static func handleCode(code:String, codeType: String, scanType:String) -> String {
        
        if  "qr" == scanType {
            
            if (code.contains("geo") || code.contains("http")
                || code.contains("vcard") || code.contains("VCARD")) {
                
                return ""
            } else {
                // add qr check
                return code
            }
        } else {
            
            if codeType == "AVMetadataObjectTypeEAN13Code" {
                if (code.contains("0") && code.characters.count > 1){
                    return code.substring(from: code.index(code.startIndex, offsetBy: 1))
                }
            }
            
            return code
            
        }
        
        return code
    }

}
