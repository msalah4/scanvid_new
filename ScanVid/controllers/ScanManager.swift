//
//  ScanManager.swift
//  ScanVid
//
//  Created by Mohammed Salah on 4/28/17.
//  Copyright © 2017 ScanVid. All rights reserved.
//

import UIKit
import RealmSwift


class ScanManager: NSObject, NetworkScanSearchDelegate, YTSearchResultDelegate {
    
    var ytSearch:YTSearchController?
    var delegate:ScanResultsDelegate?
    var requestManager:RequestController?
    var title = ""
    var code = ""
    var type = ""
    var videoID = ""
    
    init(scanDelegate: ScanResultsDelegate) {
        super.init()
        
        self.delegate = scanDelegate
        self.requestManager = RequestController ()
        
    }
    
    func submitBarcodeScanWithCode(code:String) {
        self.code = code
        self.type = Constants.ScanType.BAR_CODE
        requestManager?.searchForBarCode(code: code, deleg: self)
    }
    
    func submitQrcodeScanWithCode(code:String) {
        self.code = code
        self.type = Constants.ScanType.QR_CODE
        requestManager?.searchForQrCode(code: code, deleg: self)
    }
    
    
    
    func requestSuccessWithData (title:String, videoId:String){
        
        if (title.isEmpty || videoId.isEmpty) {
            self.delegate?.codeNotFound()
        } else {
            self.delegate?.codeFound()
            self.title = title
            self.videoID = videoId
            ytSearch = YTSearchController()
            ytSearch?.submitSearch(text: title, withDelegate: self)
        }
        
    }
    
    func requestFailedWithError (error:String){
        self.delegate?.networkError()
    }
    
    func searchResults (mainVideo:YTVideo, videos :Array<YTVideo>) {
        // record new scan
        let scanItem = ScanCode ()
        scanItem.codeID.append(self.code)
        scanItem.codeType.append(self.type)
        scanItem.itemTitle.append(self.title)
        scanItem.mainVideo = mainVideo
        scanItem.listVideos.append(objectsIn:videos)
        
        let realm = try! Realm()
        try! realm.write {
            realm.add(scanItem)
        }
        
        // show next view with results
        let nextViewController = (self.delegate as! HomeViewController).storyboard?.instantiateViewController(withIdentifier: Constants.Views.YT_RESULTS)
        (self.delegate as! HomeViewController).navigationController?.pushViewController(nextViewController!, animated: true)
        
        (nextViewController as! YTSearchResultDelegate).searchResults(mainVideo: mainVideo, videos: videos)
        
    }

}
