//
//  YTSearchProtocols.swift
//  ScanVid
//
//  Created by Mohammed Salah on 4/29/17.
//  Copyright © 2017 ScanVid. All rights reserved.
//

import Foundation

protocol YTSearchResultDelegate {
    
    func searchResults (mainVideo:YTVideo, videos :Array<YTVideo>)
    
}
