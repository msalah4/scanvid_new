//
//  SeachManager.swift
//  ScanVid
//
//  Created by Mohammed Salah on 4/28/17.
//  Copyright © 2017 ScanVid. All rights reserved.
//

import UIKit
import RealmSwift

class SeachManager: NSObject, YTSearchResultDelegate{
    
    var ytSearch:YTSearchController?
    var ytSearchViewController:SearchViewController?
    var ytSearchText:String?
    var nextViewID:String?
    
    init(searchViewController: SearchViewController, nxtView:String) {
        super.init()
        
        self.ytSearchViewController = searchViewController
        self.nextViewID = nxtView
    }
    
    func submitSearch(text:String)  {
        
        self.ytSearchText = text
        ytSearch = YTSearchController()
        ytSearch?.submitSearch(text: text, withDelegate: self)
        
    }
    
    func searchResults (mainVideo:YTVideo, videos :Array<YTVideo>){
        
        // insert in database
        let searchItem = SearchItem()
        searchItem.text = self.ytSearchText!
        searchItem.title = mainVideo.title
        searchItem.mainVideo = mainVideo
        searchItem.listVideos.append(objectsIn: videos)
        
        let realm = try! Realm()
        try! realm.write {
            realm.add(searchItem)
        }
        
        // show next view with results
        let nextViewController = self.ytSearchViewController?.storyboard?.instantiateViewController(withIdentifier: self.nextViewID!)
        self.ytSearchViewController?.navigationController?.pushViewController(nextViewController!, animated: true)
        
        (nextViewController as! YTSearchResultDelegate).searchResults(mainVideo: mainVideo, videos: videos)
        
    }

}
