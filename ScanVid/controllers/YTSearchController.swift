//
//  YTSearchController.swift
//  ScanVid
//
//  Created by Mohammed Salah on 4/28/17.
//  Copyright © 2017 ScanVid. All rights reserved.
//

import UIKit



class YTSearchController: NSObject {
    
    let MAX_RESULTS = 25
    let LANG = "de"
    
    var service:GTLRYouTubeService?
    var query:GTLRYouTubeQuery_SearchList?
    var delegate:YTSearchResultDelegate?
    
    

    func submitSearch(text:String, withDelegate YTDelegate: YTSearchResultDelegate)  {
        
        self.delegate = YTDelegate
        // initailize api
        service = GTLRYouTubeService()
        service?.apiKey = Constants.YT.Api_Key
        query = GTLRYouTubeQuery_SearchList.query(withPart: "id,snippet")
        
        query?.q = text
        query?.maxResults = UInt(MAX_RESULTS)
        query?.relevanceLanguage = LANG
        query?.type = "video"
        query?.videoSyndicated = "true"
        query?.regionCode = LANG
        query?.order = "relevance"
        
        // submit the query
//        var listOfIds = nil
        
        service?.executeQuery(query!, completionHandler: { (ticket, object , error) in

            if (error == nil) {
                
               let videoList = object as! GTLRYouTube_SearchListResponse
                
                self.featchVideosAdditionalData(videos: videoList)
            }
        })
    }
    
    
    
    func featchVideosAdditionalData(videos : GTLRYouTube_SearchListResponse)  {
        
        let videoQuery = GTLRYouTubeQuery_VideosList.query(withPart: "id, snippet, contentDetails, statistics, player")
        let allVideos = videos.items! as Array<GTLRYouTube_SearchResult>
        videoQuery.identifier = (allVideos.map({ $0.identifier?.videoId}) as NSArray).componentsJoined(by: ",")
            //(((videos.items) as! NSArray).value(forKey: "identifier.videoId") as! NSArray).componentsJoined(by: ",")
        
        service?.executeQuery(videoQuery, completionHandler: { (ticket, object, error) in
            
            if (error == nil) {
                let videoList = object as! GTLRYouTube_VideoListResponse
                
                if (videoList.items?.count)! > 0 {
                    let mainVideo = self.ytVideoParser(video: (videoList.items?[0])!)
                    var listOfVideos = Array<YTVideo> ()
                    videoList.items?.remove(at: 0)
                    
                    for video in videoList.items!{
                        let newVideo = self.ytVideoParser(video: video)
                        listOfVideos.append(newVideo)
                    }
                    
                    // notify manager
                    if(self.delegate != nil){
                     self.delegate?.searchResults(mainVideo: mainVideo, videos: listOfVideos)
                        self.delegate = nil
                    }
                    
                }
            }
            
        })
        
    }
    
    func ytVideoParser(video: GTLRYouTube_Video) -> YTVideo {
        var ytVideo = YTVideo ()
        
        ytVideo.channel = (video.snippet?.channelTitle)!
        ytVideo.videoDuration = (video.contentDetails?.duration)!
        ytVideo.numberOfviews = (video.statistics?.viewCount?.stringValue)!
        ytVideo.thumbnail = (video.snippet?.thumbnails?.defaultProperty?.url)!
        ytVideo.title = (video.snippet?.title)!
        ytVideo.ytid = (video.identifier)!
        
        if let thumb = (video.snippet?.thumbnails?.standard?.url) {
            ytVideo.Hthumbnail = thumb
        }
        
        if let thumb = (video.snippet?.thumbnails?.medium?.url) {
            ytVideo.Hthumbnail = thumb
        }
        
        if let thumb = (video.snippet?.thumbnails?.high?.url) {
            ytVideo.Hthumbnail = thumb
        }
        
        return ytVideo
    }
    
}
