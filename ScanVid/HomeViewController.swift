//
//  ViewController.swift
//  ScanVid
//
//  Created by Mohammed Salah on 4/21/17.
//  Copyright © 2017 ScanVid. All rights reserved.
//

import RealmSwift
import AVFoundation
import UIKit

class HomeViewController: UIViewController , AVCaptureMetadataOutputObjectsDelegate, ScanResultsDelegate {
    
    
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var scanManager: ScanManager?
    
    
    @IBOutlet weak var scannerView: UIView!
    @IBOutlet weak var upperView: UIView!
    @IBOutlet weak var bottomView: UIView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        scanManager = ScanManager(scanDelegate:self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        startScanning()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        stopScanning()
    }
    
    override func viewWillLayoutSubviews() {
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
    }
    
    @IBAction func toggleFlash() {
        if let device = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo), device.hasTorch {
            do {
                try device.lockForConfiguration()
                let torchOn = !device.isTorchActive
                try device.setTorchModeOnWithLevel(1.0)
                device.torchMode = torchOn ? .on : .off
                device.unlockForConfiguration()
            } catch {
                print("error")
            }
        }
    }
    
//    func applyGradientTransiparentEffcetOnView (view targetView : UIView , isFromTop : Bool) {
//        
////        let gradientLayer = CAGradientLayer()
////        gradientLayer.frame = targetView.bounds
////        gradientLayer.colors = [UIColor.white, UIColor.black, UIColor.red]
////        
////        let yStartPoint = isFromTop ? targetView.frame.size.height : 0;
////        let yEndPoint = isFromTop ? 0 : targetView.frame.size.height;
////        
////        gradientLayer.startPoint = CGPoint.init(x: targetView.center.x, y: yStartPoint)
////        gradientLayer.endPoint = CGPoint.init(x: targetView.center.x, y: yEndPoint)
////        targetView.layer.mask = (gradientLayer)
//        
//        let mask = CAGradientLayer()
//        mask.startPoint = CGPoint.init(x: 0.5, y:0.0)
//        mask.endPoint = CGPoint.init(x: 0.5, y: 1.0)
//        let whiteColor = UIColor.white
//        mask.colors = [whiteColor.withAlphaComponent(0.0).cgColor,whiteColor.withAlphaComponent(1.0),whiteColor.withAlphaComponent(1.0).cgColor]
//        mask.locations = [NSNumber(value: 0.0),NSNumber(value: 0.2),NSNumber(value: 1.0)]
//        mask.frame = targetView.bounds
//        targetView.layer.mask = mask
//    }
    
    
    func startScanning () {
        captureSession = AVCaptureSession();
        
        // Set the captureDevice.
        let videoCaptureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        
        // Create input object.
        let videoInput: AVCaptureDeviceInput?
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        
        // Add input to the session.
        if (captureSession?.canAddInput(videoInput))! {
            captureSession?.addInput(videoInput)
        } else {
            scanningNotPossible()
        }
        
        // Create output object.
        let metadataOutput = AVCaptureMetadataOutput()
        
        // Add output to the session.
        if (captureSession?.canAddOutput(metadataOutput))! {
            captureSession?.addOutput(metadataOutput)
            
            // Send captured data to the delegate object via a serial queue.
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            
            // Set barcode type for which to scan: EAN-13.
            metadataOutput.metadataObjectTypes = [AVMetadataObjectTypeEAN13Code,
                                                  AVMetadataObjectTypeQRCode,
                                               AVMetadataObjectTypeUPCECode,
                                               AVMetadataObjectTypeCode39Code,
                                               AVMetadataObjectTypeCode39Mod43Code,
                                               AVMetadataObjectTypeEAN8Code,
                                               AVMetadataObjectTypeCode93Code,
                                               AVMetadataObjectTypeCode128Code,
                                               AVMetadataObjectTypePDF417Code,
                                               AVMetadataObjectTypeAztecCode,
                                               AVMetadataObjectTypeInterleaved2of5Code,
                                               AVMetadataObjectTypeITF14Code,
                                               AVMetadataObjectTypeDataMatrixCode]
            
        } else {
            scanningNotPossible()
        }
        
        
        // Add previewLayer and have it show the video data.
        
        
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession);
        videoPreviewLayer?.frame = scannerView.layer.bounds;
        videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill;
        scannerView.layer.addSublayer(videoPreviewLayer!);
        
        // Begin the capture session.
        
        captureSession?.startRunning()
        
    }
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        
        // Get the first object from the metadataObjects array.
        if let barcodeData = metadataObjects.first {
            // Turn it into machine readable code
            let barcodeReadable = barcodeData as? AVMetadataMachineReadableCodeObject;
            if let readableCode = barcodeReadable {
                // Send the barcode as a string to barcodeDetected()
//                barcodeDetected(readableCode.stringValue);
                var codeType = barcodeReadable?.type
                
                if codeType == AVMetadataObjectTypeQRCode {
                    codeType  = Constants.ScanType.QR_CODE
                } else {
                    codeType  = Constants.ScanType.BAR_CODE
                }
                
                let codeValue = ScannerCodeHandler.handleCode(code:readableCode.stringValue, codeType: (barcodeReadable?.type)! , scanType:codeType!)
                
                
                if codeType == Constants.ScanType.BAR_CODE {
                    scanManager?.submitBarcodeScanWithCode(code: codeValue)
                } else {
                    scanManager?.submitQrcodeScanWithCode(code: codeValue)
                }
                
            }
            
            // Vibrate the device to give the user some feedback.
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            
            // Avoid a very buzzy device.
            captureSession?.stopRunning()
        }
    }
    
    func stopScanning () {
        captureSession?.stopRunning();
    }
    
    
    func scanningNotPossible() {
        // Let the user know that scanning isn't possible with the current device.
        let alert = UIAlertController(title: "Can't Scan.", message: "Let's try a device equipped with a camera.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
        captureSession = nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func codeFound (){
        
    }
    
    func codeNotFound (){
        
    }
    
    func networkError (){
        
    }
    
    
    @IBAction func scanEvaluated(_ sender: Any) {
        
        scanManager?.submitBarcodeScanWithCode(code: "8690757000020")
    }


}

