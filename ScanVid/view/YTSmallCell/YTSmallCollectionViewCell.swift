//
//  YTSmallCollectionViewCell.swift
//  ScanVid
//
//  Created by Mohammed Salah on 4/27/17.
//  Copyright © 2017 ScanVid. All rights reserved.
//

import UIKit

class YTSmallCollectionViewCell: CardCollectionViewCell {

    @IBOutlet weak var videoThumbnailImg: UIImageView!
    
    @IBOutlet weak var videoTitle: UILabel!
    
    @IBOutlet weak var videoDesc: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "YTSmallCollectionViewCell", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! YTSmallCollectionViewCell
    }

}
