//
//  HistoryViewController.swift
//  ScanVid
//
//  Created by Mohammed Salah on 5/1/17.
//  Copyright © 2017 ScanVid. All rights reserved.
//

import UIKit
import RealmSwift
class HistoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    let realm = try! Realm()
    let results = try! Realm().objects(ScanCode.self)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }
    
    
    // Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
    // Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! YTTableViewCell
        
        
        let object = results[indexPath.row]
        
        cell.videoTitle.text = object.itemTitle
        cell.videoDetials.text = String.localizedStringWithFormat("%s . %i views", (object.mainVideo?.channel)!, (object.mainVideo?.numberOfviews)!)
        cell.thumbnail.sd_setImage(with: URL(string: (object.mainVideo?.thumbnail)!), placeholderImage: UIImage(named: "placeholder.png"))

        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectedSearch = results[indexPath.row]
        let mainVid = selectedSearch.mainVideo
        let videos = Array(selectedSearch.listVideos)
        
        
        let nextViewController = (self).storyboard?.instantiateViewController(withIdentifier: Constants.Views.YT_RESULTS)
        (self).navigationController?.pushViewController(nextViewController!, animated: true)
        
        (nextViewController as! YTSearchResultDelegate).searchResults(mainVideo: mainVid!, videos: videos)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
