//
//  YTTableViewCell.swift
//  ScanVid
//
//  Created by Mohammed Salah on 5/1/17.
//  Copyright © 2017 ScanVid. All rights reserved.
//

import UIKit

class YTTableViewCell: UITableViewCell {

    @IBOutlet weak var thumbnail:UIImageView!
    @IBOutlet weak var videoTitle:UILabel!
    @IBOutlet weak var videoDetials:UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
