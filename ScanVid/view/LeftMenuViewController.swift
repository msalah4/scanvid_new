//
//  LeftMenuViewController.swift
//  ScanVid
//
//  Created by Mohammed Salah on 4/22/17.
//  Copyright © 2017 ScanVid. All rights reserved.
//

import UIKit

class LeftMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{

    let SHOW_RATIO:CGFloat  = 0.75
    let CELL_HEIGHT:CGFloat  = 44
    let CELL_SETTINGS_HEIGHT:CGFloat  = 126
    let CELL_ID = "Cell"
    let CELL_SETTINGS_ID = "SETTINGS_Cell"
    
    let menu = ["GALLARY", "HISTORY", "SETTINGS", "FAQ", "GO TO WEBSITE"]
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        
        let cellNib = UINib(nibName: "LeftMenuTableViewCell", bundle: nil)
        let settingsCellNib = UINib(nibName: "SettingsTableViewCell", bundle: nil)
        
        tableView.register(cellNib, forCellReuseIdentifier: CELL_ID)
        tableView.register(settingsCellNib, forCellReuseIdentifier: CELL_SETTINGS_ID)
        tableView.tableFooterView = UIView()
        
    }
    
    override func viewDidLayoutSubviews() {
        custamizeMenu()
        
        
//        var frame =  containerView.frame
//        frame.size.width = self.view.frame.size.width * SHOW_RATIO
//        containerView.frame = frame
    }
    
    func custamizeMenu() {
        SideMenuManager.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        
//        SideMenuManager.menuWidth = 310.67
        SideMenuManager.menuFadeStatusBar = false
        SideMenuManager.menuPresentMode = .menuSlideIn
    }
    @IBAction func toggleMenu(_ sender: Any) {
        SideMenuManager.menuRightNavigationController?.dismiss(animated: true, completion: nil)
    }
    
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 5;
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
       
        var cell = tableView.dequeueReusableCell(withIdentifier: CELL_ID) as! LeftMenuTableViewCell
       
        if indexPath.row == 2 {
            cell = tableView.dequeueReusableCell(withIdentifier: CELL_SETTINGS_ID) as! LeftMenuTableViewCell
        }
        
        cell.cellTitle.text = menu[indexPath.row]
        cell.backgroundColor = UIColor.clear
        
        return cell
    }
    
     public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 2 {
            return CELL_SETTINGS_HEIGHT
        } else {
            return CELL_HEIGHT
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
