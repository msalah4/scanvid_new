//
//  SearchViewController.swift
//  ScanVid
//
//  Created by Mohammed Salah on 4/25/17.
//  Copyright © 2017 ScanVid. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift
import SDWebImage

class SearchViewController: UIViewController, UITextViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource {

    let CELL_ID = "yt_cell"
    
    
    var searchManager: SeachManager?
    
    @IBOutlet weak var searchtextView: MSSearchView!
    @IBOutlet weak var animationLayer: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var deleteTextBtn: UIButton!
    @IBOutlet weak var upperSearchTextField: UITextField!
    @IBOutlet weak var upperSearchView: UIView!
    @IBOutlet weak var lowerSearchH: NSLayoutConstraint!
    @IBOutlet weak var searchContainer: MSInnerShadowView!
    @IBOutlet weak var realSearchHight: NSLayoutConstraint!
    var listOfSearchVideos:Results<SearchItem>!
    
    let realm = try! Realm()
    let results = try! Realm().objects(SearchItem.self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchManager = SeachManager(searchViewController: self,nxtView: Constants.Views.YT_RESULTS)
        let realm = try! Realm()
        listOfSearchVideos = realm.objects(SearchItem.self)
        let array = Array(realm.objects(SearchItem.self))
        
        let arrayOfTitles = array.map({$0.title})
        
        let count = arrayOfTitles.count
        for i in 0 ..< count {
        NSLog("Titles: %s", arrayOfTitles[i])
        }
        
//        let searchItem:SearchItem = array[0]
//        
//        NSLog("Title: %s", (searchItem.value(forKey: "text") as! String))
//        NSLog("Text: %s", (searchItem.value(forKey: "title") as! String))
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        searchtextView.layoutSubviews()
        searchtextView.delegate = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
    
        if let numberOfVideos = listOfSearchVideos {
            if (numberOfVideos.count % 2 == 1 && (section > numberOfVideos.count / 2)){
                return 1
            } else {
                return 2
            }

        }
        return 0
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    var cell = collectionView.dequeueReusableCell(withReuseIdentifier: CELL_ID, for: indexPath as IndexPath) as! YTSmallCollectionViewCell
        
        if cell.videoTitle == nil {
            cell = YTSmallCollectionViewCell.instanceFromNib() as! YTSmallCollectionViewCell
        }
        
        let currentIndex = (indexPath.section * 2) + indexPath.row

        let object = results[currentIndex]
        
        cell.videoTitle.text = object.title
//        cell.videoDesc.text = String.localizedStringWithFormat("%s . %s views", (object.mainVideo?.channel)!, (object.mainVideo?.numberOfviews)!)
        cell.videoThumbnailImg.sd_setImage(with: URL(string: (object.mainVideo?.thumbnail)!), placeholderImage: UIImage(named: "placeholder.png"))

    
    return cell
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int{
        
        if listOfSearchVideos == nil {
            return 0
        }
        
        let numberOfVideos = listOfSearchVideos?.count
        
        if (numberOfVideos! % 2 == 1 ){
            return (numberOfVideos! / 2) + 1
        } else {
            return numberOfVideos! / 2
        }
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        if self.searchtextView.text == "SEARCH" {
        
            self.searchtextView.text = ""
        }
        
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.searchtextView.resignFirstResponder()
        
        

    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if range.length == 0 {
            if text == "\n" {
                self.searchtextView.resignFirstResponder()
                // start search logic
                searchManager?.submitSearch(text: searchtextView.text)
                return false;
            }
        }
        
        return true
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool  {
        
        for v in self.view.subviews {
            v.resignFirstResponder()
        }
        
        self.searchtextView.resignFirstResponder()
        
        // start search logic
        searchManager?.submitSearch(text: searchtextView.text!)
        
        return false
    }
    
    
    func startAnimateSearch() {
        
        // 1- show background image
        self.view.bringSubview(toFront: animationLayer)
        animationLayer.isHidden = false
        animationLayer.alpha = 0.5
        
        let currentSearchViewH = self.searchtextView.frame.size.height
        
        // 2- hide back textview
        UIView.animate(withDuration: 0.5) { 
//            var frame = self.SearchTextView.frame
//            frame.size.height = 0
//            self.SearchTextView.frame = frame
            
            self.lowerSearchH.constant = 0
            self.realSearchHight.constant = currentSearchViewH
            self.upperSearchView.layoutIfNeeded()
            self.upperSearchView.layoutSubviews()
            
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let currentIndex = (indexPath.section * 2) + indexPath.row
            let selectedSearch = results[currentIndex]
            let mainVid = selectedSearch.mainVideo
            let videos = Array(selectedSearch.listVideos)
        
        
            let nextViewController = (self).storyboard?.instantiateViewController(withIdentifier: Constants.Views.YT_RESULTS)
            (self).navigationController?.pushViewController(nextViewController!, animated: true)
        
            (nextViewController as! YTSearchResultDelegate).searchResults(mainVideo: mainVid!, videos: videos)
    }
    



    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
