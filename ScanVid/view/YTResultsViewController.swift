//
//  YTResultsViewController.swift
//  ScanVid
//
//  Created by Mohammed Salah on 4/29/17.
//  Copyright © 2017 ScanVid. All rights reserved.
//

import UIKit
import iCarousel
import youtube_ios_player_helper

class YTResultsViewController: UIViewController, YTSearchResultDelegate, iCarouselDataSource, iCarouselDelegate, YTPlayerViewDelegate {

    
    var listOfVideos:Array<YTVideo>?
    var mainVideo:YTVideo?
    var ytPlayer:YTPlayerView?
    
    @IBOutlet weak var thumb: UIImageView!
    @IBOutlet weak var titleView: CardView!
    @IBOutlet weak var titleViewupperConstraint: NSLayoutConstraint!
    @IBOutlet weak var listOfVideosContainer: UIView!
    @IBOutlet weak var icarouselView: iCarousel!
    @IBOutlet weak var videoTitle: UILabel!
    @IBOutlet weak var videoDesc: UILabel!
    
    var isViewUpdated = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        icarouselView.type = iCarouselType.rotary
        icarouselView.delegate = self
        icarouselView.dataSource = self
        
    }
    
    override func viewDidLayoutSubviews() {
        setupUI()
        isViewUpdated = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        
        guard ((listOfVideos) != nil) else {
            return 0
        }
        return (listOfVideos?.count)!;
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        
        var cell:YTSmallCollectionViewCell
        
        if view == nil {
            cell = YTSmallCollectionViewCell.instanceFromNib() as! YTSmallCollectionViewCell
            
        } else {
            cell = view as! YTSmallCollectionViewCell
        }
        
        let object = listOfVideos?[index]
        
        cell.videoTitle.text = object?.title
//        cell.videoDesc.text = String.localizedStringWithFormat("%s . %s views", (object!.channel), (object!.numberOfviews))
        cell.videoThumbnailImg.sd_setImage(with: URL(string: (object!.thumbnail)), placeholderImage: UIImage(named: "placeholder.png"))
        
        return cell
    }
    
    
    func searchResults (mainVideo:YTVideo, videos :Array<YTVideo>){
        
        self.mainVideo = mainVideo
        self.listOfVideos = videos
        setupUI()
    }
    
    func setupUI() {
        
        guard videoTitle != nil else {
            return
        }
        
        if self.mainVideo != nil  {
            let title:String = (mainVideo?.title)! as String
            let channel:String = (mainVideo?.channel)! as String
            let numberOfviews:String = (mainVideo?.numberOfviews)! as String
            
            videoTitle.text = title
//            self.videoDesc.text = String.localizedStringWithFormat("%s . %s views", (mainVideo?.channel)!, (mainVideo?.numberOfviews)!)
            
            if mainVideo?.Hthumbnail != nil {
                thumb.sd_setImage(with: URL(string: (mainVideo?.thumbnail)!), placeholderImage: UIImage(named: "placeholder.png"))
            } else {
                thumb.sd_setImage(with: URL(string: (mainVideo?.thumbnail)!), placeholderImage: UIImage(named: "placeholder.png"))
            }
            
            
//            videoDesc.text = String.localizedStringWithFormat("%s . %s views", channel, numberOfviews)
            
        }
        
        if !isViewUpdated {
            titleViewupperConstraint.constant -= 50
            titleView.layoutIfNeeded()
            self.icarouselView.reloadData()
        }
        
        
        
    }
    
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        
        // check for the internet
        
        let videoToPlay:YTVideo = listOfVideos![index]
        self.playVideoWithID(videoID: videoToPlay.ytid)
        
    }
    
    func playVideoWithID(videoID:String) {
        self.ytPlayer = YTPlayerView()
        self.ytPlayer?.frame = CGRect.init(x: 0, y: 65, width: self.view.frame.size.width, height: self.view.frame.size.height - 65)
        let controlers = ["controls":1, "playsinline":1, "autohide":1,
                          "showinfo":1, "modestbranding":1]
        self.ytPlayer?.delegate = self
        self.view.addSubview(self.ytPlayer!)
        self.ytPlayer?.load(withVideoId: videoID, playerVars: controlers)
        self.ytPlayer?.playVideo()
    }

    func playerView(_ playerView: YTPlayerView, receivedError error: YTPlayerError) {
        self.ytPlayer?.removeFromSuperview()
    }
    
    func playerView(_ playerView: YTPlayerView, didChangeTo state: YTPlayerState){
        
        if state == YTPlayerState.ended {
            self.ytPlayer?.removeFromSuperview()
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
