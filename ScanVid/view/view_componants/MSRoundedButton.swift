//
//  MSRoundedButton.swift
//  ScanVid
//
//  Created by Mohammed Salah on 4/22/17.
//  Copyright © 2017 ScanVid. All rights reserved.
//

import UIKit

//@IBDesignable
class MSRoundedButton: UIButton {

    override func layoutSubviews() {
        super.layoutSubviews()
        
        updateCornerRadius()
    }
    
    @IBInspectable var rounded: Bool = false {
        didSet {
            updateCornerRadius()
        }
    }
    
    func updateCornerRadius() {
        layer.cornerRadius = rounded ? frame.size.height / 2 : 0
    }

}
