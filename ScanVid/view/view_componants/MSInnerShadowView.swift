//
//  MSInnerShadowView.swift
//  ScanVid
//
//  Created by Mohammed Salah on 4/25/17.
//  Copyright © 2017 ScanVid. All rights reserved.
//

import UIKit

//@IBDesignable
class MSInnerShadowView: UIView {

    @IBInspectable var cornerRadius: CGFloat = 2
    @IBInspectable var shadowOffset	 = CGSize.init(width: 4.0, height: 2.0)
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 3
    @IBInspectable var shadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0.5
    
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        
        let value : CGFloat = 3
        
        let path = UIBezierPath.init()
        path.move(to: CGPoint.zero)
        path.addLine(to: CGPoint.init(x: value, y: value))
        path.addLine(to: CGPoint.init(x: value, y: frame.size.height - value))
        path.addLine(to: CGPoint.init(x: frame.size.width - value, y: frame.size.height - value))
        path.addLine(to: CGPoint.init(x: frame.size.width - value   , y: value))
//        path.addCurve(to: CGPoint.init(x: 0, y:  frame.size.height + 15),
//                      controlPoint1: CGPoint.init(x: frame.size.width - 15, y: frame.size.height), controlPoint2: CGPoint.init(x: 15, y: frame.size.height))
        path.close()
        
        let shadowPath = UIBezierPath(roundedRect: frame, cornerRadius: cornerRadius)
        
        layer.masksToBounds = false
        layer.shadowOffset = shadowOffset
        layer.shadowColor = shadowColor?.cgColor
//        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = path.cgPath
        
    }

}
