//
//  MSSearchView.swift
//  ScanVid
//
//  Created by Mohammed Salah on 4/25/17.
//  Copyright © 2017 ScanVid. All rights reserved.
//

import UIKit

//@IBDesignable
class MSSearchView: UITextView {

    
    @IBInspectable var cornerRadius: CGFloat = 2
    
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 3
    @IBInspectable var shadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0.5
    
    
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        
        updateCornerRadius()
        layer.masksToBounds = false
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
        textContainer.maximumNumberOfLines = 1
        textContainer.lineBreakMode = .byTruncatingTail
        
        
        // centerlize text vertically and horizotally
        var topCorrect : CGFloat = (frame.height / 2) - (contentSize.height / 2)
        topCorrect = topCorrect < 0.0 ? 0.0 : topCorrect
        contentInset = UIEdgeInsetsMake(topCorrect,10,0,0)
    }
    
    @IBInspectable var rounded: Bool = false {
        didSet {
            updateCornerRadius()
        }
    }
    
    func updateCornerRadius() {
        layer.cornerRadius = rounded ? frame.size.height / 2 : 0
    }
    
    
    
}
