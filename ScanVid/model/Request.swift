//
//  Request.swift
//  ScanVid
//
//  Created by Mohammed Salah on 4/29/17.
//  Copyright © 2017 ScanVid. All rights reserved.
//

import UIKit

enum RequestType {
    case search,
     scan
}

class Request: NSObject {
    
    var requestType: RequestType?
    var value:String?
    var extra:String?
    

}

