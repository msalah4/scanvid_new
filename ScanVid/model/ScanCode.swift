//
//  ScanCode.swift
//  ScanVid
//
//  Created by Mohammed Salah on 4/27/17.
//  Copyright © 2017 ScanVid. All rights reserved.
//

import UIKit
import RealmSwift

class ScanCode: Object {
    
    public dynamic var codeType = ""
    public dynamic var codeID = ""
    public dynamic var itemTitle = ""
    public dynamic var mainVideo : YTVideo? = nil
    public let listVideos = List<YTVideo> ()
}
