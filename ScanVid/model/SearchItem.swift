//
//  SearchItem.swift
//  ScanVid
//
//  Created by Mohammed Salah on 4/27/17.
//  Copyright © 2017 ScanVid. All rights reserved.
//

import UIKit
import RealmSwift

class SearchItem: Object {
    
    dynamic var text = ""
    dynamic var title = ""
    dynamic var mainVideo : YTVideo?
    let listVideos = List<YTVideo> ()
}
