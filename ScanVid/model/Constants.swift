//
//  Constants.swift
//  ScanVid
//
//  Created by Mohammed Salah on 4/29/17.
//  Copyright © 2017 ScanVid. All rights reserved.
//

import UIKit

struct Constants {
    struct YT {
        static let Api_Key = "AIzaSyBfab3QFXYJeBlVhIE8m-lnPg_58qLV9sM"
    }
    
    struct Views {
       static let YT_RESULTS = "YTresults"
        static let SEARCH = "search"
    }
    
    struct ScanType {
        static let QR_CODE:String = "qr"
        static let BAR_CODE:String = "bar"
    }
    
    struct Request {
        static let QR_URL = "http://188.226.163.193/ios/FromQRcodeFast.php?qr="
        static let BAR_URL = "http://188.226.163.193/ios/FromDBlangEnglishTableFast.php?lang=German&br="
    }
}
