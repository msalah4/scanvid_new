//
//  YTVideo.swift
//  ScanVid
//
//  Created by Mohammed Salah on 4/27/17.
//  Copyright © 2017 ScanVid. All rights reserved.
//

import UIKit
import RealmSwift

class YTVideo: Object {

    public dynamic var ytid = ""
    public dynamic var videoDuration = ""
    public dynamic var desc = ""
    public dynamic var thumbnail = ""
    public dynamic var Hthumbnail = ""
    public dynamic var title = ""
    public dynamic var channel = ""
    public dynamic var numberOfviews = ""

}
